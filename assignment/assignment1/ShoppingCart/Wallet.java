import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.concurrent.locks.ReentrantLock;

 
  

public class Wallet {
    private final ReentrantLock locker=new ReentrantLock();
    //private final Unlock locker = new ReentrantLock();
    //locker.lock();
    //locker.unlock();
   /**
    * The RandomAccessFile of the wallet file
    */  
   private RandomAccessFile file;

   /**
    * Creates a Wallet object
    *
    * A Wallet object interfaces with the wallet RandomAccessFile
    */
    public Wallet () throws Exception {
	this.file = new RandomAccessFile(new File("wallet.txt"), "rw");
    }

   /**
    * Gets the wallet balance. 
    *
    * @return                   The content of the wallet file as an integer
    */
    public int getBalance() throws IOException 
    {
        //FileLock lock = file.getChannel().lock();
        this.file.seek(0);
        //lock.release();
        return Integer.parseInt(this.file.readLine());
    }

   /**
    * Sets a new balance in the wallet
    *
    * @param  newBalance          new balance to write in the wallet
    */
    public void setBalance(int newBalance) throws Exception {
    this.file.setLength(0);
    String str = new Integer(newBalance).toString()+'\n';
    this.file.writeBytes(str);
    }

    

    public void safeWithdraw(int b, int valueToWithdraw) throws Exception
    {
        //FileLock lock = file.getChannel().lock();
        locker.lock();
    	//int balance= this.getBalance();
    	if(b >=valueToWithdraw){
    		this.setBalance(b-valueToWithdraw);
            locker.unlock();
    		//return valueToWithdraw;
    	}else{
                int withdrawnAmt = getBalance();
                setBalance(0);System.out.println("Your balance is less to perform this transaction");
               // System.out.println("Your old balance : " + withdrawnAmount+" credits");
                System.out.println("Your new balance : " + this.getBalance()+" credits");
                locker.unlock();
                //return withdrawnAmount;

    		}
    	
    }

    public void safeDeposit(int valueToDeposit) throws Exception
    {
        locker.lock();
        int balance = this.getBalance();
        setBalance(balance + valueToDeposit);
        System.out.println("Your new balance after safeDeposit is: " + this.getBalance()+ "credits");
        locker.unlock();

    }
    

   /**
    * Closes the RandomAccessFile in this.file
    */
    public void close() throws Exception {
	this.file.close();
    }
}
