import java.util.Date;
public aspect ShoppingCartAspect{
	pointcut safeWithdraw(int b, int valueToWithdraw, Wallet wallet): call(* Wallet.safeWithdraw(..)) && args(b, valueToWithdraw) && target(wallet);
	before(int b,int valueToWithdraw, Wallet wallet): safeWithdraw(b, valueToWithdraw, wallet){
          if(valueToWithdraw>b) {
          System.out.println("Your balance is less than the requested amount");
          }   
    }
    
}
