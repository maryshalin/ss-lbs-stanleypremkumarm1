<?php 
  require("../classes/auth.php");
  require("header.php");
  require("../classes/db.php");
  require("../classes/phpfix.php");
  require("../classes/post.php");
  
  
?>

<?php 
  $token = $_GET["nocsrftoken"];
  if (!isset($token) or ($token != $_SESSION["nocsrftoken"])){
    echo "<script>alert('CSRF attack detected')</script>";
    die(); 
  }
  $post = Post::delete((int)($_GET["id"]));
  header("Location: /admin/index.php");
?>

