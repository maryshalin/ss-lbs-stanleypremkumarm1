<?php 
  require("../classes/auth.php");
  require("header.php");
  require("../classes/db.php");
  require("../classes/phpfix.php");
  require("../classes/post.php");
  $rand = bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;

  $post = Post::find($_GET['id']);
  if (isset($_POST['title'])) {
    $token = $_POST["nocsrftoken"];
      if (!isset($token) or ($token != $_SESSION["nocsrftoken"])){
        echo "<script>alert('CSRF attack detected')</script>";
        die();
      }

    $post->update($_POST['title'], $_POST['text']);
  } 
?>
  
  <form action="edit.php?id=<?php echo htmlentities($_GET['id']);?>" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>">
    Title: 
    <input type="text" name="title" value="<?php echo htmlentities($post->title); ?>" /> <br/>
    Text: 
      <textarea name="text" cols="80" rows="5">
        <?php echo htmlentities($post->text); ?>
       </textarea><br/>

    <input type="submit" name="Update" value="Update">

  </form>

<?php
  require("footer.php");

?>

